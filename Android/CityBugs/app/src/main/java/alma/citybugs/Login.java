package alma.citybugs;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Login extends ActionBarActivity {
    //Declaramos nuestros objetos
    private Toolbar toolbar;
    private EditText txtUsuario;
    private EditText txtPass;
    private CheckBox cbRecordar;
    private Button btnAcceder;

    private ProgressDialog progressDialog;
    private ArrayList<NameValuePair> pares;

    private GestorUsuario gestorUsuario;
    public String respuesta;
    private Compartido compartido;
    private String usuario,password;
    private SharedPreferences loginPreferences;
    private SharedPreferences.Editor loginPrefsEditor;
    private Boolean saveLogin;

    private static int IDUSER;

    private class HiloComprobar extends AsyncTask<Integer, Integer, Integer> {

        @Override
        protected Integer doInBackground(Integer... arg0) {
            respuesta = gestorUsuario.isUser(pares);
            return null;
        }

        @Override
        protected void onPostExecute(Integer result) {
            progressDialog.dismiss();
            if (respuesta != null && respuesta.indexOf("ok") != -1) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(txtUsuario.getWindowToken(), 0);

                usuario = txtUsuario.getText().toString();
                password = txtPass.getText().toString();

                if (cbRecordar.isChecked()) {
                    loginPrefsEditor.putBoolean("saveLogin", true);
                    loginPrefsEditor.putString("username", usuario);
                    loginPrefsEditor.putString("password", password);
                    loginPrefsEditor.commit();
                } else {
                    loginPrefsEditor.clear();
                    loginPrefsEditor.commit();
                }
                guardamosId(respuesta);
                msg("Bienvenido " + txtUsuario.getText().toString());
                Intent intento = new Intent(Login.this, CrearIncidencia.class);
                compartido.setId_usuario(IDUSER);
                intento.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intento);
            } else {
                msg("Usuario o contraseña incorrecta...");
            }

        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        compartido = new Compartido();
        gestorUsuario = new GestorUsuario(); //Instanciamos el objeto de la clase GestorUsuario
        loginPreferences = getSharedPreferences("loginPrefs", MODE_PRIVATE);
        loginPrefsEditor = loginPreferences.edit();

        //Asociamos los atributos de la clase a los elementos del layout que correspondan
        txtUsuario = (EditText) findViewById(R.id.txtUsuario);
        txtPass = (EditText) findViewById(R.id.txtPass);
        btnAcceder = (Button) findViewById(R.id.btnLocalizacion);
        cbRecordar = (CheckBox) findViewById(R.id.cbRecordar);

        saveLogin = loginPreferences.getBoolean("saveLogin", false);
        if (saveLogin == true) {
            txtUsuario.setText(loginPreferences.getString("username", ""));
            txtPass.setText(loginPreferences.getString("password", ""));
            cbRecordar.setChecked(true);
        }

        btnAcceder.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Este evento se iniciara cuando pulsen el boton "Acceder"
                //Lo primero que vamos a comprobar antes de nada, es si hay conexion a internet
                if (getServicio()) {
                    //Ahora comprobamos si las cajas de texto estan rellenadas.
                    //Para ello llamamos al metodo compruebaCajas
                    if (compruebaCajas()) {
                        //Aqui comprobamos si el usuario y la contraseña son correctos.
                        //Para ello, haremos una consulta al servidor.
                        pares = new ArrayList<NameValuePair>(2); //Aqui se almacena el nick y el pass
                        pares.add(new BasicNameValuePair("name", txtUsuario.getText().toString()));
                        pares.add(new BasicNameValuePair("pass", txtPass.getText().toString()));
                        progressDialog = ProgressDialog.show(Login.this, null, "Comprobando...", true, false);
                        new HiloComprobar().execute();
                    } else {
                        //Las cajas no estan rellenas. Lanzamos un mensaje avisando
                        msg("Rellene todos los campos por favor.");
                    }
                } else {
                    //No dispone de conexion a internet. Lanzamos un mensaje avisando.
                    msg("No dispone de conexion a internet en estos momentos.");
                }
            }
        });
    }

    public void guardamosId(String respuesta) {
        // TODO Auto-generated method stub
        try {
            JSONArray jsa = new JSONArray(respuesta);
            JSONObject jso = jsa.getJSONObject(1);
            IDUSER = jso.getInt("_id");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent();
        switch (item.getItemId()) {
            case R.id.nuevoUsuario:
                intent.setClass(Login.this, Registro.class);
                startActivity(intent);
                finish();
                return true;
            case R.id.info:
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle("Información");
                alertDialog.setMessage("CityBugs ha sido desarrollada para la asignatura Gestión de Sistemas de Información " +
                        "de la Escuela Superior de Informática por: Álvaro Fernández Villa y " +
                        "María del Pilar Moreno Duque");
                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Add your code for the button here.
                    }
                });
                alertDialog.setIcon(R.drawable.ic_info);
                alertDialog.show();
                return true;
            case R.id.salir:
                finish();
                System.exit(0);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //Este metodo lo que hace es comprobar si disponemos de conexion a Internet.
    //En caso afirmativo devuelve un true, en caso contrario devuelve un false.
    public boolean getServicio() {
        ConnectivityManager cm = (ConnectivityManager) getBaseContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni != null && ni.isAvailable() && ni.isConnected())
            return true;
        else
            return false;
    }

    //Este metodo lo que hace es lanzar un mensaje por pantalla con el texto
    //que se le pasa por parametro a traves de la variable msg
    public void msg(String msg) {
        Toast.makeText(getBaseContext(), msg, Toast.LENGTH_SHORT).show();
    }

    //Este metodo lo que hace es comprobar si las cajas de texto de usuario y contraseña
    //estan rellenas. Si lo estan, devuelve true, si no, devuelve false
    public boolean compruebaCajas() {
        if (txtUsuario.getText().length() > 0 && txtPass.getText().length() > 0)
            return true; //Se puede
        else
            return false; //No se puede
    }
}

