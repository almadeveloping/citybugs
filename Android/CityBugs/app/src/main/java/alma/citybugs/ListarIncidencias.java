package alma.citybugs;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class ListarIncidencias  extends ActionBarActivity implements View.OnClickListener {
    private Toolbar toolbar;
    private LinearLayout lltareas; //Linear Layout que contrendra una lista de linears
    private ProgressDialog progressDialog; //Progress dialog que aparecera cuando se entre a este layout para cargar las incidencias
    private ArrayList<Incidencia> incidencias; //Todas las incidencias se guardaran en este a
    private GestorIncidendia gestorIncidendia; //Objeto del tipo AdaptadorInci que contiene un metodo para descargar las incidencias de la BBDD
    private Compartido compartido;//Objeto del tipo Compartido para datos en comun de los layout

    //Hilo que descargara las incidencias
    private class HiloGetIncidencias extends AsyncTask<Integer, Integer, Integer> {

        @Override
        protected Integer doInBackground(Integer... arg0) {
            //Recogemos las incidencias
            incidencias=gestorIncidendia.getIncidencias();
            return null;
        }

        @Override
        protected void onPostExecute(Integer result) {
            //Una vez tenemos recogidas las incidencias, paramos el progressDialog
            progressDialog.dismiss();
            //Comprobamos si nos ha llegado mas de 0 incidencias
            if(incidencias.size()>0){
                //Llenamos el linear con los nombres de las incidencias
                llenamoslinear();
            }else{
                //Si no han llegado incidencias, mostramos el siguiente mensaje:
                msg("No hay incidencias");
            }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listar_incidencias);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("Listar Incidencias");
        setSupportActionBar(toolbar);

        //Instanciamos compartido
        compartido=new Compartido();
        //msg(""+c.getId_usuario());
        //Instanciamos el objeto AdaptadorInci
        gestorIncidendia=new GestorIncidendia();
        //Asociamos el LinearLoyout con su elemento correspondiente del layout
        lltareas=(LinearLayout)findViewById(R.id.lltareas);
        //Llamamos al metodo getIncidencias
        getIncidencias();
        //Metodo clic del boton incidencias
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent();
        switch (item.getItemId()) {
            case R.id.crearIncidencia:
                intent.setClass(ListarIncidencias.this, CrearIncidencia.class);
                startActivity(intent);
                finish(); // finaliza la actividad
                return true;
            case R.id.listarIncidencias:
                intent.setClass(ListarIncidencias.this, ListarIncidencias.class);
                startActivity(intent);
                finish(); // finaliza la actividad
                return true;
            case R.id.verMapaGlobal:
                intent.setClass(ListarIncidencias.this, VerMapaGlobal.class);
                startActivity(intent);
                finish(); // finaliza la actividad
                return true;
            case R.id.info:
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle("Información");
                alertDialog.setMessage("CityBugs ha sido desarrollada para la asignatura Gestión de Sistemas de Información " +
                        "de la Escuela Superior de Informática por: Álvaro Fernández Villa y " +
                        "María del Pilar Moreno Duque");
                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Add your code for the button here.
                    }
                });
                alertDialog.setIcon(R.drawable.ic_info);
                alertDialog.show();
                return true;
            case R.id.logout:
                intent.setClass(ListarIncidencias.this, Login.class);
                startActivity(intent);
                finish(); // finaliza la actividad
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //Metodo que llenara el linear con las incidencias
    public void llenamoslinear() {
        //limpiamos el layout
        lltareas.removeAllViews();

        //Nos recorremos el array de incidencias
        for(int i=0;i<incidencias.size();i++){
            //Creamos un ImageView que contrendra la imagen de la flecha
            ImageView iv=new ImageView(getApplicationContext());
            //Asignamos la imagen
            iv.setImageResource(R.drawable.ic_right);
            //Asignamos el tamaño a la imagen (40dp X 40dp)
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams((int) convertToPixel(40), (int) convertToPixel(40));
            //Asignamos este parametro de layout a la ImageView
            iv.setLayoutParams(layoutParams);
            //Le asignamos un tag (para saber en cual nos han pulsado despues)
            iv.setTag(""+i);
            //Le ponemos un listener
            iv.setOnClickListener(this);
            //Creamos el linear layout que contendra la ImageView y el TextView
            LinearLayout mil=new LinearLayout(getApplicationContext());
            //Le asignamos un padding segun corresponda
            if(i==0){
                mil.setPadding(5, 5, 5, 5);
            }else{
                mil.setPadding(5, 0, 5, 5);
            }
            //Asignamos al linear una alineacion centrada
            mil.setGravity(Gravity.CENTER);
            //Asignamos el fondo al linear. El fondo esta en un XML llamado fondo.xml (drawable)
            mil.setBackgroundResource(R.drawable.fondo);
            //Creamos un TextView
            TextView tv=new TextView(ListarIncidencias.this);
            //Asignamos el nombre de la incidencia al texto del textview
            tv.setText(incidencias.get(i).getName_inc());
            //Le asignamos un padding segun corresponda
            if(i==0){
                tv.setPadding(0, 2, 2, 2);
            }else{
                tv.setPadding(0, 0, 2, 2);
            }
            //Le asignamos una altura
            tv.setHeight((int) convertToPixel(40));
            //Le asignamos un tamaño de letra
            tv.setTextSize(18);
            //Le asignamos un ancho
            tv.setWidth((int) convertToPixel(250));
            //Alineacion vertical centrada:
            tv.setGravity(Gravity.CENTER_VERTICAL);
            //Añadimos al linear el TextView
            mil.addView(tv);
            //Añadimos al linear el ImageView
            mil.addView(iv);
            //Añadimos el linear al linear general
            lltareas.addView(mil);
        }

    }
    //Metodo que activa el Progress Dialog y llama al HiloGetIncidencias
    private void getIncidencias() {
        progressDialog=ProgressDialog.show(ListarIncidencias.this, null, "Descargando incidencias...", true, false);
        new HiloGetIncidencias().execute();
    }

    //Este metodo lo que hace es lanzar un mensaje por pantalla con el texto
    //que se le pasa por parametro a traves de la variable msg
    public void msg(String msg){
        Toast.makeText(getBaseContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    //Este metodo es llamado cada vez que pulsan un elemento del linear
    public void onClick(View v) {
        //Recogemos el tag en forma de String
        String n=(String) v.getTag();
        //Lo pasamos a Integer
        int i=Integer.parseInt(n);
        //Asignamos al atributo incidencia del objeto compartido la incidencia que corresponda
        compartido.setIncidencia(incidencias.get(i));
        //Creamos el intent de detalles
        Intent intento = new Intent(ListarIncidencias.this, VerIncidencia.class);
        //cargamos la pantalla de detalles de la incidencia
        startActivity(intento);
    }
    //Este metodo convierte dp en pixeles
    private float convertToPixel(int n){
        Resources r = getResources();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, n, r.getDisplayMetrics());
        return px;
    }
}




