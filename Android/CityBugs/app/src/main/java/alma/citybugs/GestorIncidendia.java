package alma.citybugs;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class GestorIncidendia {
    //URL's que se van a usar tanto para insertar como para consultar
    private String urlSetIncidencia="http://citybugs.esy.es/CityBugs/setInci.php";
    private String urlGetIncidencia="http://citybugs.esy.es/CityBugs/getInci.php";

    //Metodo que inserta una incidencia a atraves de una lista de claves/valor
    //pasadas por parametro

    public String setIncidencia(List<NameValuePair> nameValuePairs) {
        String respuesta = null;
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(urlSetIncidencia);
        try {
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            respuesta = httpclient.execute(httppost, responseHandler);
        } catch (Exception e) {
        }
        return respuesta;
    }

    //Metodo que devuelve todas las incidencias en un Array
    public ArrayList<Incidencia> getIncidencias() {
        ArrayList<Incidencia> incidencias = new ArrayList<Incidencia>();
        String respuesta = null;

        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httpget = new HttpPost(urlGetIncidencia);
            try {
                ResponseHandler<String> responseHandler = new BasicResponseHandler();
                respuesta = httpclient.execute(httpget, responseHandler);

            } catch (Exception e) {
            }
            // La URL nos devuelve un String que era el Array Asociativo con las
            // tuplas de la select convertido a JSON
            JSONArray jsa = new JSONArray(respuesta);
            // Contruimos los objetos y los metemos en el ArrayList
            for (int i = 0; i < jsa.length(); i++) {

                JSONObject jso = jsa.getJSONObject(i);

                Incidencia incidencia = new Incidencia();
                incidencia.set_id(jso.getInt("_id"));
                incidencia.setName_inc(jso.getString("name_inc"));
                incidencia.setFecha(jso.getString("fecha"));
                incidencia.setComentario(jso.getString("comentario"));
                incidencia.setId_usr(jso.getInt("id_usr"));
                incidencia.setLatitud(jso.getString("latitud"));
                incidencia.setLongitud(jso.getString("longitud"));
                incidencia.setImagen(jso.getString("imagen"));
                incidencia.setEstado(jso.getString("estado"));
                incidencias.add(incidencia);
            }
        } catch (Exception e) {
        }
        return incidencias;
    }
}


