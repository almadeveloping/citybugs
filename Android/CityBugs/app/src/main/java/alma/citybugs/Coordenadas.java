package alma.citybugs;

public class Coordenadas {

    private String latitud;
    private String longitud;

    public Coordenadas() {
        super();
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

}