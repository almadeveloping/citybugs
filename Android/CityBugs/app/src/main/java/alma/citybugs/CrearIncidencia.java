package alma.citybugs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class CrearIncidencia extends ActionBarActivity {
    private Toolbar toolbar;
    private TextView txtFecha;
    private EditText txtNombre;
    private EditText txtComentario;
    private Button btnLocalizacion;
    private Button btnFotografia;
    private Button btnAceptar;

    private ProgressDialog progressDialog;
    private Bundle bundle; //Clase que sirve para contener tipos primitivos y objetos de otras clases.
    private int iduser;//id del usuario conectado
    private long captureTime; //para la foto
    private double latitud=0.0;

    private Coordenadas cord; //Objeto Coordenadas que almacenara las coordenadas GPS
    private Intent locatorService = null; //Objeto Intent

    private String photoPath; //Path donde se almacena la foto
    private GestorIncidendia gestorIncidendia; //El adaptador de Incidencias contiene metodos para insertar incidencias en la BBDD
    private String respuesta; //Aqui recogeremos la respuesta del servidor al insertar una incidencia
    private ArrayList<NameValuePair> pares; //pares valor para insertar la incidencia
    private Compartido compartido;

    private Uri output;

    int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    ImageView preview;

    //Hilo de subida de datos
    private class UploaderFoto extends AsyncTask<Integer, Integer, Integer> {
        //Con esta variable booleana vamos a controlar que los dos pasos
        private boolean subida=true;

        @Override
        protected Integer doInBackground(Integer... arg0) {
            //Hacemos un insert en la BBDD
            respuesta=gestorIncidendia.setIncidencia(pares);
            //Si la respuesta no es null, y tiene un ok, es que el insert se ha hecho correctamente
            if (respuesta!=null && respuesta.indexOf("ok")!=-1) {
                //Como el insert se ha hecho bien, pasamos a subir la foto:
                try {
                    //Abrimos el la foto para enviarla
                    FileInputStream fis = new FileInputStream(compartido.photoPath);
                    //Creamos un objeto de la clase HttpFileUploader
                    HttpFileUploader htfu = new HttpFileUploader("http://citybugs.esy.es/CityBugs/update.php", photoPath);
                    //Iniciamos el hilo de ejecucion de la clase HttpFileUploader
                    htfu.doStart(fis);
                } catch (FileNotFoundException e) {
                    //Error subiendo la foto
                    subida = false;
                    e.printStackTrace();
                }
            }else{
                //Error haciendo el insert
                subida=false;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Integer result) {
            //Paramos el Progress Dialog
            progressDialog.dismiss();
            if(subida){
                msg("Incidencia reportada correctamente.");
                reseteo();
            }else{
                msg("Error a la hora de reportar la incidencia");
            }

        }
    }

    //El hilo HiloCoordenadas se ejecutara a traves del metodo startService()
    //Este hilo es el que se encarga de buscar las coordenadas GPS
    private class HiloCoordenadas extends AsyncTask<Integer, Integer, Integer> {
        //Progres Dialogo que mostraremos mientras obtenemos las coordenadas GPS
        private ProgressDialog progDailog = null;
        //Atributo que almacenará la latitud (0.0 por defecto)
        public double lati = 0.0;
        //Atributo que almacenará la longitud (0.0 por defecto)
        public double longi = 0.0;
        //Objeto de la clase LocationManager
        public LocationManager mLocationManager;
        //LocationListener modificado:
        public MyLocationListener myLocationListener;
        //Boleano que tendra true si el GPS esta activo y false si no lo esta
        private boolean activo=true;
        @Override
        //Metodo que se ejecuta despues de onPreExecute()
        protected Integer doInBackground(Integer... arg0) {
            // TODO Auto-generated method stub
            //Hasta que el atributo contenga algo distinto a 0.0 no saldremos de aqui.
            //Ademas, si el GPS no esta activo, el bucle terminará
            while (this.lati == 0.0 && activo) {
            }
            return null;
            //Ahora vamos al metodo onPostExecute

        }

        @Override
        //Metodo del hilo que se ejecuta cuando se cancela
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
            System.out.println("Cancelado por el usuario");
            progDailog.dismiss();
            mLocationManager.removeUpdates(myLocationListener);
        }

        @Override
        //Metodo del hilo que se ejecuta el primero
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            //Instanciamos nuestro LocationListener modificado
            myLocationListener = new MyLocationListener();
            //Instanciamos nuestro LocationManager
            mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            //Comenzamos a buscar
            //Existen dos metodos: GPS_PROVIDER y NETWORK_PROVIDER
            //GPS_PROVIDER:MAS LENTO PERO MAS EXACTO
            //NETWORK_PROVIDER:MAS RAPIDO PERO MENOS EXACTO
            mLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, 0, 0,
                    myLocationListener);
            //Instanciamos el ProgressDialog
            progDailog = new ProgressDialog(CrearIncidencia.this);
            //Colocamos un mensaje:
            progDailog.setMessage("Cargando...");
            //Lo hacemos indeterminado
            progDailog.setIndeterminate(true);
            //Lo ponemos como no cancelable
            progDailog.setCancelable(false);
            //Lo mostramos por pantalla
            progDailog.show();
            //Ahora vamos al metodo doInBackground()
        }

        @Override
        //Metodo que va despues del doInBackground()
        protected void onPostExecute(Integer result) {
            //Paramos el progress dialog
            progDailog.dismiss();
            //Mostramos en una tostada las coordenadas:
            if(activo){
                msg("Sus coordenadas son: "+cord.getLatitud()+"/"+cord.getLongitud());
            }else{
                msg("Por favor, active el GPS");
            }
        }

        //###############CLASE MODIFICADA LOCATIONLISTENER####################
        public class MyLocationListener implements LocationListener {
            //Esta clase se encarga de obtener las coordenadas
            @Override
            public void onLocationChanged(Location location) {

                int lat = (int) location.getLatitude(); // LATITUD
                int log = (int) location.getLongitude(); // LONGITUD
                int acc = (int) (location.getAccuracy()); //PRECISION

                String info = location.getProvider(); //Obtenemos el proovedor
                try {

                    lati = location.getLatitude();
                    longi = location.getLongitude();
                    cord.setLatitud(lati+""); //Guardamos la Latitud
                    cord.setLongitud(longi+"");//Guardamos la Longitud
                    latitud=lati;
                    //Dejamos de buscar:
                    mLocationManager.removeUpdates(myLocationListener);
                } catch (Exception e) {
                    //Si ocurre algun problema paramos el progress dialog y mostramos por pantallla
                    //que no es posible recibir la localizacion
                    progDailog.dismiss();
                    Toast.makeText(getApplicationContext(), "Imposible recibir localización"
                            , Toast.LENGTH_LONG).show();
                }

            }
            //Este metodo se ejecuta si el GPS no esta activo
            @Override
            public void onProviderDisabled(String provider) {
                Log.i("OnProviderDisabled", "OnProviderDisabled");
                activo=false; //Ponemos la variable que controla el bucle a false
                //Dejamos de buscar
                mLocationManager.removeUpdates(myLocationListener);
            }

            @Override
            public void onProviderEnabled(String provider) {
                Log.i("onProviderEnabled", "onProviderEnabled");
                activo=true;
            }

            @Override
            public void onStatusChanged(String provider, int status,
                                        Bundle extras) {
                Log.i("onStatusChanged", "onStatusChanged");

            }

        }//###############FIN CLASE MODIFICADA LOCATIONLISTENER####################
    }//#############FIN DEL HILO HILOCOORDENADAS###################


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.crear_incidencia);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("Crear Incidencia");
        setSupportActionBar(toolbar);

        compartido=new Compartido();
        gestorIncidendia=new GestorIncidendia();
        //Recogemos el _id del usuario que nos manda la activity Main a traves de un bundle
        bundle = getIntent().getExtras();
        //Guardamos en iduser el id del usuario que almacenamos en compartido
        iduser=compartido.getId_usuario();

        //Instanciamos el objeto Coordenadas para poder usarlo
        cord=new Coordenadas();
        //Asociamos los atributos de la clase con los elementos del layout correspondientes
        txtFecha=(Button)findViewById(R.id.btnFecha);
        txtNombre=(EditText)findViewById(R.id.txtNombre);
        txtComentario=(EditText)findViewById(R.id.txtComentario);
        btnLocalizacion=(Button)findViewById(R.id.btnLocalizacion);
        btnFotografia=(Button)findViewById(R.id.btnFotografia);
        btnAceptar=(Button)findViewById(R.id.btnAceptar);

        //Al textview fecha le asignamos la fecha actual con el metodo getFechaActual()
        txtFecha.setText(getFechaActual());
        //Creamos un listener para el textview
        txtFecha.setOnClickListener(new View.OnClickListener() {

            @SuppressLint("NewApi")
            @Override
            public void onClick(View arg0) {
                //Enseñamos el dialogo para elegir una fecha
                crearDialogoConfirmacion().show();
            }
        });
        //Creamos un listener para el boton Localizaci�n
        btnLocalizacion.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Si no tenemos las coordenadas, las buscamos:
                if(cord.getLatitud()==null){
                    if (!startService()) {
                        CreateAlert("Error!", "El servicio no ha podido comenzar");
                    }
                }else{
                    //Si ya tenemos las coordenadas, no hace falta buscarlas de nuevo:
                    msg("Sus coordenadas son: "+cord.getLatitud()+"/"+cord.getLongitud());
                }
            }
        });

        File path = Environment.getExternalStorageDirectory();

        captureTime=System.currentTimeMillis();
        photoPath = path.getAbsolutePath()
                + "/DCIM/Camera/"+iduser+"_" + captureTime + ".jpg";
        compartido.setPhotoPath(photoPath);


        //Creamos un listener para el boton Fotografia
        btnFotografia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                final CharSequence[] items = { "Usar la Cámara", "Cancelar" };
                AlertDialog.Builder builder = new AlertDialog.Builder(CrearIncidencia.this);
                builder.setTitle("Añadir Foto");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (items[item].equals("Usar la Cámara")) {
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            File foto = new File(photoPath);
                            Log.d("photo", photoPath);
                            Uri output = Uri.fromFile(foto);
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, output);
                            startActivityForResult(Intent.createChooser(intent, "Hacer Foto"), REQUEST_CAMERA);
                        } else if (items[item].equals("Usar la Libreria")) {
                            Intent intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            intent.setType("image/*");
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, output);
                            startActivityForResult( Intent.createChooser(intent, "Seleccionar Foto"),SELECT_FILE);
                        } else if (items[item].equals("Cancelar")) {
                            dialog.dismiss();
                            photoPath=null; //Reseteamos el path
                            compartido.setPhotoPath(photoPath);
                            Toast.makeText(getApplicationContext(), "No se ha realizado la foto", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                builder.show();
            }
        });

        preview = (ImageView) findViewById(R.id.preview);
        preview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(compartido.getPhotoPath() != null){
                    Intent intento = new Intent(CrearIncidencia.this, FotoCrear.class);
                    startActivity(intento);
                }
            }
        });

        //Metodo del boton aceptar
        btnAceptar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //Antes de nada, volvemos a comprobar si hay internet
                if(getServicio()){
                    if(photoPath!=null && txtNombre.getText().length()!=0 && txtComentario.getText().length()!=0 && latitud!=0.0 ){
                        pares=getParesIncidencia();
                        progressDialog=ProgressDialog.show(CrearIncidencia.this, null, "Subiendo...", true, false);
                        new UploaderFoto().execute();
                    }else{
                        msg("Todos los campos son obligatorios");
                    }
                }else{
                    //Si no dispone de internet, mostramos el mensaje:
                    msg("No dispone de conexion a internet en estos momentos.");
                    //Ademas, devolvemos al Main al usuario:
                    Intent intento = new Intent(CrearIncidencia.this, Login.class);
                    startActivity(intento);
                }
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent();
        switch (item.getItemId()) {
            case R.id.crearIncidencia:
                intent.setClass(CrearIncidencia.this, CrearIncidencia.class);
                startActivity(intent);
                finish(); // finaliza la actividad
                return true;
            case R.id.listarIncidencias:
                intent.setClass(CrearIncidencia.this, ListarIncidencias.class);
                startActivity(intent);
                finish(); // finaliza la actividad
                return true;
            case R.id.verMapaGlobal:
                intent.setClass(CrearIncidencia.this, VerMapaGlobal.class);
                startActivity(intent);
                finish(); // finaliza la actividad
                return true;
            case R.id.info:
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle("Información");
                alertDialog.setMessage("CityBugs ha sido desarrollada para la asignatura Gestión de Sistemas de Información " +
                        "de la Escuela Superior de Informática por: Álvaro Fernández Villa y " +
                        "María del Pilar Moreno Duque");
                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Add your code for the button here.
                    }
                });
                alertDialog.setIcon(R.drawable.ic_info);
                alertDialog.show();
                return true;
            case R.id.logout:
                intent.setClass(CrearIncidencia.this, Login.class);
                startActivity(intent);
                finish(); // finaliza la actividad
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //Este metodo resetea todos los valores.
    //Es llamado cuando se termina de notificar una incidencia.
    public void reseteo() {
        txtNombre.setText("");
        txtComentario.setText("");
        latitud=0.0;
        captureTime=0;
        photoPath=null;
        cord.setLatitud(null);
        cord.setLongitud(null);
    }

    //Este metodo devuelve el array de pares/valor que se necesita para notificar la incidencia
    protected ArrayList<NameValuePair> getParesIncidencia() {
        ArrayList<NameValuePair> pares=new ArrayList<NameValuePair>(7);
        pares.add(new BasicNameValuePair("name", txtNombre.getText().toString()));
        pares.add(new BasicNameValuePair("fecha", txtFecha.getText().toString()));
        pares.add(new BasicNameValuePair("come", txtComentario.getText().toString()));
        pares.add(new BasicNameValuePair("lati", cord.getLatitud()));
        pares.add(new BasicNameValuePair("longi", cord.getLongitud()));
        pares.add(new BasicNameValuePair("foto", iduser+"_" + captureTime + ".jpg"));
        pares.add(new BasicNameValuePair("idus", iduser+""));
        return pares;
    }

    //Este metodo crea un dialogo para poder elegir una fecha.
    private Dialog crearDialogoConfirmacion(){
        //Instanciamos el dialogo
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        //Creamos un Scroll horizontal ya que el DatePicker es demasiado grande
        //De esta forma podremos desplazarnos horizontalmente por el DatePicker
        HorizontalScrollView h=new HorizontalScrollView(getBaseContext());
        //Creamos el DatePicker. Que es el que nos permite escoger otra fecha.
        final DatePicker dp=new DatePicker(getBaseContext());
        //Si queremos poner una fecha minima (La del dia de hoy) descomentar las siguientes tres lineas:
//	    Calendar cal = Calendar.getInstance();
//	    cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
//	    dp.setMinDate(cal.getTimeInMillis());
        //Añadimos el DatePicker al scroll horizontal
        h.addView(dp);
        //Asiganmos un titulo al dialogo, en este caso pedimos que elijan una fecha
        builder.setTitle("Elige una fecha:");
        //Añadimos el scroll horizontal (que a su vez contiene el DatePicker) al dialogo
        builder.setView(h);
        //Añadimos el boton Aceptar:
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Log.i("Dialogos", "Confirmación Aceptada.");
                //Asignamos al TextView de la fecha la fecha elegida en el DatePicker
                txtFecha.setText(dameDia(dp.getDayOfMonth())+"/"+dameMes(dp.getMonth())+"/"+dp.getYear());
                //Cerramos el dialogo
                dialog.cancel();
            }
            //Este pequeño metodo tiene dos funciones:
            //el metodo getMonth del DatePicker devuelve los meses del 0 al 11, por lo que le sumamos uno.
            //añadimos un 0 delante de los meses menores que 10
            private String dameMes(int month) {
                month++;
                if(month<10)
                    return "0"+month;
                else
                    return month+"";
            }
            //Este metodo añade un 0 a los dias menores que 10
            private String dameDia(int dayOfMonth) {
                if(dayOfMonth<10)
                    return "0"+dayOfMonth;
                else
                    return dayOfMonth+"";

            }
        });
        //Añadimos el boton Cancelar:
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Log.i("Dialogos", "Confirmación Cancelada.");
                //Cerramos el dialogo sin hacer nada
                dialog.cancel();
            }
        });
        //Creamos y devolvemos el dialogo
        return builder.create();
    }
    //Este metodo devuelve una cadena con la fecha actual en formato dd/MM/yyyy
    public static String getFechaActual() {
        Date ahora = new Date();
        SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");
        return formateador.format(ahora);
    }
    //Este metodo recibe una cadena por parametro y la enseña por pantalla en forma de Toast
    public void msg(String msg){
        Toast.makeText(getBaseContext(), msg, Toast.LENGTH_SHORT).show();
    }
    //Este metodo para el servicio de busqueda de coordenadas GPS
    public boolean stopService() {
        if (this.locatorService != null) {
            this.locatorService = null;
        }
        return true;
    }

    //Este metodo arranca el HiloCoordenadas que se encarga de buscar las coordenadas GPS.
    //Ademas, este metodo devuelve true si el hilo ha arrancado correctamente.
    public boolean startService() {
        try {
            HiloCoordenadas fetchCordinates = new HiloCoordenadas();
            fetchCordinates.execute();
            return true;
        } catch (Exception error) {
            return false;
        }

    }
    //Metodo que devuelve un AlertDialogo. Se usa la hora de mostrar mensajes de alerta
    //para notificar cualquier problema.
    public AlertDialog CreateAlert(String title, String message) {
        AlertDialog alert = new AlertDialog.Builder(this).create();
        alert.setTitle(title);
        alert.setMessage(message);
        return alert;

    }
    //Este metodo se ejecutara despues de hacer la fotografia
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        preview = (ImageView) findViewById(R.id.preview);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                preview.setImageBitmap(BitmapFactory.decodeFile(photoPath));
                compartido.setPhotoPath(photoPath);
            }else if (requestCode == SELECT_FILE) {
                Uri selectedImageUri = data.getData();
                photoPath = getPath(selectedImageUri, CrearIncidencia.this);
                compartido.setPhotoPath(photoPath);
                preview.setImageBitmap(BitmapFactory.decodeFile(photoPath));
            }
        }else{
            photoPath=null; //Reseteamos el path
            compartido.setPhotoPath(photoPath);
            Toast.makeText(getApplicationContext(), "No se ha realizado la foto", Toast.LENGTH_SHORT).show();
        }

    }

    //Este metodo lo que hace es comprobar si disponemos de conexion a Internet.
    //En caso afirmativo devuelve un true, en caso contrario devuelve un false.
    public boolean getServicio() {
        ConnectivityManager cm = (ConnectivityManager) getBaseContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni != null && ni.isAvailable() && ni.isConnected())
            return true;
        else
            return false;
    }

    public String getPath(Uri uri, Activity activity) {
        String[] projection = { MediaStore.MediaColumns.DATA };
        Cursor cursor = activity.managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

}


