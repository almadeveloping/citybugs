package alma.citybugs;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

public class FotoCrear extends ActionBarActivity  {

    private Toolbar toolbar;
    private Compartido compartido = new Compartido();
    private String photoPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ver_foto);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("Ver Foto");
        setSupportActionBar(toolbar);

        photoPath = compartido.getPhotoPath();

        ZoomFoto imagen = (ZoomFoto)findViewById(R.id.image_view);
        imagen.setImageBitmap(BitmapFactory.decodeFile(photoPath));

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent();
        switch (item.getItemId()) {
            case R.id.crearIncidencia:
                intent.setClass(FotoCrear.this, CrearIncidencia.class);
                startActivity(intent);
                finish(); // finaliza la actividad
                return true;
            case R.id.listarIncidencias:
                intent.setClass(FotoCrear.this, ListarIncidencias.class);
                startActivity(intent);
                finish(); // finaliza la actividad
                return true;
            case R.id.verMapaGlobal:
                intent.setClass(FotoCrear.this, VerMapaGlobal.class);
                startActivity(intent);
                finish(); // finaliza la actividad
                return true;
            case R.id.info:
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle("Información");
                alertDialog.setMessage("CityBugs ha sido desarrollada para la asignatura Gestión de Sistemas de Información " +
                        "de la Escuela Superior de Informática por: Álvaro Fernández Villa y " +
                        "María del Pilar Moreno Duque");
                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Add your code for the button here.
                    }
                });
                alertDialog.setIcon(R.drawable.ic_info);
                alertDialog.show();
                return true;
            case R.id.logout:
                intent.setClass(FotoCrear.this, Login.class);
                startActivity(intent);
                finish(); // finaliza la actividad
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}