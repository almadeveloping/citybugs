package alma.citybugs;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class FotoVer extends ActionBarActivity {

    private Toolbar toolbar;
    private Compartido compartido = new Compartido();

    private ZoomFoto imageView;
    private String URL = ("http://citybugs.esy.es/CityBugs/imagenes/"+compartido.getIncidencia().getImagen());

    private class CargaImagenes extends AsyncTask<String, Void, Bitmap>{

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();

            progressDialog = new ProgressDialog(FotoVer.this);
            progressDialog.setMessage("Cargando Imagen");
            progressDialog.setCancelable(true);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();

        }

        @Override
        protected Bitmap doInBackground(String... params) {
            // TODO Auto-generated method stub
            Log.i("doInBackground" , "Entra en doInBackground");
            String url = params[0];
            Bitmap imagen = descargarImagen(url);
            return imagen;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

            imageView.setImageBitmap(result);
            progressDialog.dismiss();
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ver_foto);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("Ver Foto");
        setSupportActionBar(toolbar);

        imageView = (ZoomFoto) findViewById(R.id.image_view);
        CargaImagenes nuevaTarea = new CargaImagenes();
        nuevaTarea.execute(URL);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent();
        switch (item.getItemId()) {
            case R.id.crearIncidencia:
                intent.setClass(FotoVer.this, CrearIncidencia.class);
                startActivity(intent);
                finish(); // finaliza la actividad
                return true;
            case R.id.listarIncidencias:
                intent.setClass(FotoVer.this, ListarIncidencias.class);
                startActivity(intent);
                finish(); // finaliza la actividad
                return true;
            case R.id.verMapaGlobal:
                intent.setClass(FotoVer.this, VerMapaGlobal.class);
                startActivity(intent);
                finish(); // finaliza la actividad
                return true;
            case R.id.info:
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle("Información");
                alertDialog.setMessage("CityBugs ha sido desarrollada para la asignatura Gestión de Sistemas de Información " +
                        "de la Escuela Superior de Informática por: Álvaro Fernández Villa y " +
                        "María del Pilar Moreno Duque");
                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Add your code for the button here.
                    }
                });
                alertDialog.setIcon(R.drawable.ic_info);
                alertDialog.show();
                return true;
            case R.id.logout:
                intent.setClass(FotoVer.this, Login.class);
                startActivity(intent);
                finish(); // finaliza la actividad
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    private Bitmap descargarImagen (String imageHttpAddress){
        URL imageUrl = null;
        Bitmap imagen = null;
        try{
            imageUrl = new URL(imageHttpAddress);
            HttpURLConnection conn = (HttpURLConnection) imageUrl.openConnection();
            conn.connect();
            imagen = BitmapFactory.decodeStream(conn.getInputStream());
        }catch(IOException ex){
            ex.printStackTrace();
        }

        return imagen;
    }



    //Este metodo recibe una cadena por parametro y la enseña por pantalla en forma de Toast
    public void msg(String msg){
        Toast.makeText(getBaseContext(), msg, Toast.LENGTH_SHORT).show();
    }




}