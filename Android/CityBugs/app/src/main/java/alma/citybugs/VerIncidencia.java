package alma.citybugs;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import static android.view.View.OnClickListener;

public class VerIncidencia extends ActionBarActivity {
    private Toolbar toolbar;
    private Compartido compartido = new Compartido(); //Objeto Compartido que contiene el ID del usuario y la Incidencia seleccionada
    private TextView txtNombre;
    private TextView txtFecha;
    private TextView txtComentario;
    private TextView txtLocalizacion;
    private ImageView imgFoto;
    private ImageView imgMapa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ver_incidencia);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("Ver Incidencia");
        setSupportActionBar(toolbar);

        //Asignamos los layout
        txtNombre=(TextView)findViewById(R.id.txtNombre);
        txtFecha=(TextView)findViewById(R.id.txtFecha);
        txtComentario=(TextView)findViewById(R.id.txtComentario);
        txtLocalizacion=(TextView)findViewById(R.id.txtLocalizacion);
        imgFoto=(ImageView)findViewById(R.id.imgFoto);
        imgMapa=(ImageView)findViewById(R.id.imgMapa);
        //Asignamos los datos
        txtNombre.setText(compartido.getIncidencia().getName_inc());
        txtFecha.setText("Fecha: "+compartido.getIncidencia().getFecha());
        txtComentario.setText("Comentario: "+compartido.getIncidencia().getComentario());
        txtLocalizacion.setText("Localización");
        //Ponemos un listener al boton de Localizacion:

        imgMapa.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                //Instanciamos un intent que llama a la clase Mapa
                Intent intento = new Intent(VerIncidencia.this, VerMapa.class);
                //Arrancamos el intent del mapa
                startActivity(intento);

            }
        });

        imgFoto.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                //Instanciamos un intent que llama a la clase FotoVer
                Intent intento = new Intent(VerIncidencia.this, FotoVer.class);
                //Arrancamos el intent de la imagen
                startActivity(intento);

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent();
        switch (item.getItemId()) {
            case R.id.crearIncidencia:
                intent.setClass(VerIncidencia.this, CrearIncidencia.class);
                startActivity(intent);
                finish(); // finaliza la actividad
                return true;
            case R.id.listarIncidencias:
                intent.setClass(VerIncidencia.this, ListarIncidencias.class);
                startActivity(intent);
                finish(); // finaliza la actividad
                return true;
            case R.id.verMapaGlobal:
                intent.setClass(VerIncidencia.this, VerMapaGlobal.class);
                startActivity(intent);
                finish(); // finaliza la actividad
                return true;
            case R.id.info:
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle("Información");
                alertDialog.setMessage("CityBugs ha sido desarrollada para la asignatura Gestión de Sistemas de Información " +
                        "de la Escuela Superior de Informática por: Álvaro Fernández Villa y " +
                        "María del Pilar Moreno Duque");
                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Add your code for the button here.
                    }
                });
                alertDialog.setIcon(R.drawable.ic_info);
                alertDialog.show();
                return true;
            case R.id.logout:
                intent.setClass(VerIncidencia.this, Login.class);
                startActivity(intent);
                finish(); // finaliza la actividad
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //Este metodo recibe una cadena por parametro y la enseña por pantalla en forma de Toast
    public void msg(String msg){
        Toast.makeText(getBaseContext(), msg, Toast.LENGTH_SHORT).show();
    }
}


