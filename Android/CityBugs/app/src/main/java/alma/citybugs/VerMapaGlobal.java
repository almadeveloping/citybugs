package alma.citybugs;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import java.util.ArrayList;

public class VerMapaGlobal extends ActionBarActivity {
    private GoogleMap mapa;
    private GestorIncidendia gestorIncidencia; //Adaptador de Incidencias
    private ProgressDialog progressDialog; //ProgresDialog para el hilo
    private ArrayList<Incidencia> incidencias; //Array que contendra las incidencias
    private Toolbar toolbar;

    private class HiloGetIncidencias extends AsyncTask<Integer, Integer, Integer> {

        @Override
        protected Integer doInBackground(Integer... arg0) {
            //Recogemos las incidencias
            incidencias=gestorIncidencia.getIncidencias();
            return null;
        }

        @Override
        protected void onPostExecute(Integer result) {
            //Una vez tenemos recogidas las incidencias, paramos el progress
            progressDialog.dismiss();
            //Comprobamos si nos ha llegado mas de 0 incidencias
            if(incidencias.size()>0){
                //Llenamos el mapa con las incidencias
                llenamosmapa();
            }else{
                //Si no han llegado incidencias, mostramos el siguiente mensaje:
                msg("No hay incidencias");
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mapa_global);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("Ver Mapa");
        setSupportActionBar(toolbar);

        //Instanciamos el Adaptador de Incidencias
        gestorIncidencia=new GestorIncidendia();
        //Instanciamos el Objeto GoogleMap
        mapa = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
        //Situamos el mapa sobre Ciudad Real
        CameraUpdate ciudadReal = CameraUpdateFactory.newLatLngZoom(new LatLng(38.9860438, -3.9269879), 12);
        //Aplicamos los cambios:
        mapa.moveCamera(ciudadReal);
        getIncidencias();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent();
        switch (item.getItemId()) {
            case R.id.crearIncidencia:
                intent.setClass(VerMapaGlobal.this, CrearIncidencia.class);
                startActivity(intent);
                finish();
                return true;
            case R.id.listarIncidencias:
                intent.setClass(VerMapaGlobal.this, ListarIncidencias.class);
                startActivity(intent);
                finish();
                return true;
            case R.id.verMapaGlobal:
                intent.setClass(VerMapaGlobal.this, VerMapaGlobal.class);
                startActivity(intent);
                finish();
                return true;
            case R.id.info:
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle("Información");
                alertDialog.setMessage("CityBugs ha sido desarrollada para la asignatura Gestión de Sistemas de Información " +
                        "de la Escuela Superior de Informática por: Álvaro Fernández Villa y " +
                        "María del Pilar Moreno Duque");
                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Add your code for the button here.
                    }
                });
                alertDialog.setIcon(R.drawable.ic_info);
                alertDialog.show();
                return true;
            case R.id.logout:
                intent.setClass(VerMapaGlobal.this, Login.class);
                startActivity(intent);
                finish(); // finaliza la actividad
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //Metodo que sirve para poner en marcha el ProgressDialog y el hilo que descarga las incidencias
    private void getIncidencias() {
        progressDialog= ProgressDialog.show(VerMapaGlobal.this, null, "Descargando incidencias...", true, false);
        new HiloGetIncidencias().execute();
    }
    //Metodo que rellena el mapa con los marcadores
    public void llenamosmapa() {
        for (int i=0;i<incidencias.size();i++){
            mostrarMarcador(Double.parseDouble(incidencias.get(i).getLatitud()), Double.parseDouble(incidencias.get(i).getLongitud()),incidencias.get(i).getName_inc(),incidencias.get(i).getComentario());
        }
    }
    //Creamos un marcador pasandole las coordenadas, el nombre y el comentario
    private void mostrarMarcador(double lat, double lng, String name, String come)
    {
        mapa.addMarker(new MarkerOptions()
                .position(new LatLng(lat, lng))
                .title("Incidencia: "+name)
                .snippet(come));

    }
    //Metodo que sirve para sacar un Toast por pantalla
    public void msg(String msg){
        Toast.makeText(getBaseContext(), msg, Toast.LENGTH_SHORT).show();
    }
}