package alma.citybugs;

import android.graphics.Bitmap;

public class Compartido {
    public static int id_usuario;
    public static Incidencia incidencia;
    public static String photoPath;
    public static Bitmap photo;

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        Compartido.id_usuario = id_usuario;
    }

    public Incidencia getIncidencia() {
        return incidencia;
    }

    public void setIncidencia(Incidencia incidencia) {
        Compartido.incidencia = incidencia;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        Compartido.photoPath = photoPath;
    }

    public Bitmap getPhoto() {
        return photo;
    }

    public void setPhoto(Bitmap photo) {
        Compartido.photo = photo;
    }
}

