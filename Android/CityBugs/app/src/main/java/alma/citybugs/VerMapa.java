package alma.citybugs;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class VerMapa extends ActionBarActivity {
    private GoogleMap mapa;
    private Compartido compartido;
    private Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ver_mapa);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("Ver Mapa");
        setSupportActionBar(toolbar);

        compartido=new Compartido();

        //Instanciamos el Objeto GoogleMap
        mapa = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
        //Situamos el mapa sobre Ciudad Real
        CameraUpdate incidencia = CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(compartido.getIncidencia().getLatitud()), Double.parseDouble(compartido.getIncidencia().getLongitud())), 16);
        //Aplicamos los cambios:
        mapa.moveCamera(incidencia);
        mostrarMarcador(Double.parseDouble(compartido.getIncidencia().getLatitud()), Double.parseDouble(compartido.getIncidencia().getLongitud()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent();
        switch (item.getItemId()) {
            case R.id.crearIncidencia:
                intent.setClass(VerMapa.this, CrearIncidencia.class);
                startActivity(intent);
                finish();
                return true;
            case R.id.listarIncidencias:
                intent.setClass(VerMapa.this, ListarIncidencias.class);
                startActivity(intent);
                finish();
                return true;
            case R.id.verMapaGlobal:
                intent.setClass(VerMapa.this, VerMapaGlobal.class);
                startActivity(intent);
                finish();
                return true;
            case R.id.info:
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle("Información");
                alertDialog.setMessage("CityBugs ha sido desarrollada para la asignatura Gestión de Sistemas de Información " +
                        "de la Escuela Superior de Informática por: Álvaro Fernández Villa y " +
                        "María del Pilar Moreno Duque");
                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Add your code for the button here.
                    }
                });
                alertDialog.setIcon(R.drawable.ic_info);
                alertDialog.show();
                return true;
            case R.id.logout:
                intent.setClass(VerMapa.this, Login.class);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //Creamos un marcador
    private void mostrarMarcador(double lat, double lng){
        mapa.addMarker(new MarkerOptions()
                .position(new LatLng(lat, lng))
                .title("Incidencia: "+compartido.getIncidencia().getName_inc())
                .snippet(compartido.getIncidencia().getComentario()));

    }

}

