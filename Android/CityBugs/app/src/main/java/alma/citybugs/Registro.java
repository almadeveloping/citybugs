package alma.citybugs;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;

public class Registro extends ActionBarActivity {
    private TextView lblLink;
    private Button btnRegistro;
    private EditText txtNombre;
    private EditText txtApellidos;
    private EditText txtTelefono;
    private EditText txtEmail;
    private EditText txtPass;

    private Toolbar toolbar;
    private ProgressDialog progressDialog;
    private GestorUsuario gestorUsuario;
    private ArrayList<NameValuePair> paresUsuario;
    private String respuesta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registro);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("Registro");
        setSupportActionBar(toolbar);

        txtNombre = (EditText) findViewById(R.id.txtNombre);
        txtApellidos = (EditText) findViewById(R.id.txtApellidos);
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        txtPass = (EditText) findViewById(R.id.txtPass);
        txtTelefono = (EditText) findViewById(R.id.txtTelefono);
        btnRegistro = (Button) findViewById(R.id.btnRegistro);
        gestorUsuario=new GestorUsuario();


        btnRegistro.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            //Antes de nada, volvemos a comprobar si hay internet
            if(getServicio()){
                if (txtNombre.getText().length() != 0 && txtApellidos.getText().length() != 0 && txtEmail.getText().length() != 0 && txtPass.getText().length() != 0 && txtTelefono.getText().length() != 0) {
                    paresUsuario = getParesUsuario();
                    respuesta = gestorUsuario.setUsuario(paresUsuario);
                    if (respuesta != null && respuesta.indexOf("ok") != -1) {
                        msg("Usuario Creado");
                        Intent intento = new Intent(Registro.this, Login.class);
                        startActivity(intento);
                    } else {
                        msg("No se ha creado el nuevo usuario");
                    }
                } else {
                    msg("Todos los campos son obligatorios");
                }
            }else{
                //Si no dispone de internet, mostramos el mensaje:
                msg("No dispone de conexion a internet en estos momentos.");
                //Ademas, devolvemos al Main al usuario:
                Intent intento = new Intent(Registro.this, Login.class);
                startActivity(intento);
                }
            }
        });

        lblLink = (TextView) findViewById(R.id.lblLink);
        lblLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intento = new Intent(Registro.this, Login.class);
                Registro.this.startActivity(intento);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_registro, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent();
        switch (item.getItemId()) {
            case R.id.volverLogin:
                intent.setClass(Registro.this, Login.class);
                startActivity(intent);
                finish();
                return true;
            case R.id.info:
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle("Información");
                alertDialog.setMessage("CityBugs ha sido desarrollada para la asignatura Gestión de Sistemas de Información " +
                        "de la Escuela Superior de Informática por: Álvaro Fernández Villa y " +
                        "María del Pilar Moreno Duque");
                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Add your code for the button here.
                    }
                });
                alertDialog.setIcon(R.drawable.ic_info);
                alertDialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //Este metodo devuelve el array de pares/valor que se necesita para notificar la incidencia
    protected ArrayList<NameValuePair> getParesUsuario() {
        ArrayList<NameValuePair> pares=new ArrayList<NameValuePair>(7);
        pares.add(new BasicNameValuePair("name", txtNombre.getText().toString()));
        pares.add(new BasicNameValuePair("ape", txtApellidos.getText().toString()));
        pares.add(new BasicNameValuePair("pass1", txtPass.getText().toString()));
        pares.add(new BasicNameValuePair("phone", txtTelefono.getText().toString()));
        pares.add(new BasicNameValuePair("email", txtEmail.getText().toString()));
        return pares;
    }

    public void reseteo() {
        txtNombre.setText("");
        txtApellidos.setText("");
        txtPass.setText("");
        txtEmail.setText("");
        txtTelefono.setText("");

    }

    public AlertDialog CreateAlert(String title, String message) {
        AlertDialog alert = new AlertDialog.Builder(this).create();
        alert.setTitle(title);
        alert.setMessage(message);
        return alert;

    }

    //Este metodo lo que hace es comprobar si disponemos de conexion a Internet.
    //En caso afirmativo devuelve un true, en caso contrario devuelve un false.
    public boolean getServicio() {
        ConnectivityManager cm = (ConnectivityManager) getBaseContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni != null && ni.isAvailable() && ni.isConnected())
            return true;
        else
            return false;
    }

    //Este metodo lo que hace es lanzar un mensaje por pantalla con el texto
    //que se le pasa por parametro a traves de la variable msg
    public void msg(String msg){
        Toast.makeText(getBaseContext(), msg, Toast.LENGTH_SHORT).show();
    }
}

