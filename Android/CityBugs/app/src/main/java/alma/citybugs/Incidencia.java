package alma.citybugs;

import android.net.Uri;

public class Incidencia {
    private int _id;
    private String name_inc;
    private String fecha;
    private String comentario;
    private int id_usr;
    private String latitud;
    private String longitud;
    private String imagen;
    private String estado;
    private Uri imageUri;

    public int get_id() {
        return _id;
    }
    public void set_id(int _id) {
        this._id = _id;
    }
    public String getName_inc() {
        return name_inc;
    }
    public void setName_inc(String name_inc) {
        this.name_inc = name_inc;
    }
    public String getFecha() {
        return fecha;
    }
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    public String getComentario() {
        return comentario;
    }
    public void setComentario(String comentario) {
        this.comentario = comentario;
    }
    public int getId_usr() {
        return id_usr;
    }
    public void setId_usr(int id_usr) {
        this.id_usr = id_usr;
    }
    public String getLatitud() {
        return latitud;
    }
    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }
    public String getLongitud() {
        return longitud;
    }
    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }
    public String getImagen() {
        return imagen;
    }
    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
    public String getEstado() {
        return estado;
    }
    public void setEstado(String estado) {
        this.estado = estado;
    }
    public void setImageUri(Uri imageUri) {
        this.imageUri = imageUri;
    }
    public Uri getImageUri() {
        return imageUri;
    }

}

