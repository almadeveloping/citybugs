package alma.citybugs;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import java.util.List;

public class GestorUsuario {
    //URL's para comprobar si el usuario es el correcto y para crear uno nuevo
    private String urlComprobar = "http://citybugs.esy.es/CityBugs/isUser.php";
    private String urlSetUser = "http://citybugs.esy.es/CityBugs/setUser.php";

    //Metodo que inserta un usuario a atraves de una lista de claves/valor
    //pasadas por parametro
    public String setUsuario(List<NameValuePair> nameValuePairs) {
        String respuesta = null;
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(urlSetUser);
        try {
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            respuesta = httpclient.execute(httppost, responseHandler);
        } catch (Exception e) {
        }
        return respuesta;
    }

        // El metodo "isUser" lo que hara es llamar a la URL "urlComprobar"
        // pasandole como parametros el nombre de usuario y la contraseña
        // esto se hará atraves del metodo POST.
        // el paso de parametros se hace mediante un array de claves/valor
    public String isUser(List<NameValuePair> nameValuePairs) {
        String respuesta = null;
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(urlComprobar);
        try {
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            respuesta = httpclient.execute(httppost, responseHandler);
        } catch (Exception e) {
        }
        return respuesta;
    }
}

