<? 
include('conectar.php'); 

$result = mysql_query("SELECT * FROM `incidencia`") or trigger_error(mysql_error()); 
?>

<!DOCTYPE html>
<html lang="es" class="no-js">

    <head>

        <meta charset="utf-8">
        <title>Incidencias</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- CSS -->
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=PT+Sans:400,700'>
        <link rel="stylesheet" href="assets/css/reset.css">
        <link rel="stylesheet" href="assets/css/supersized.css">
        <link rel="stylesheet" href="assets/css/style.css">



        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
        <script type="text/javascript">
        //<![CDATA[
        
        var map;
        
        // Ban Jelačić Square - City Center
        var center = new google.maps.LatLng(38.9899, -3.9205);
        
        var geocoder = new google.maps.Geocoder();
        var infowindow = new google.maps.InfoWindow();
        
        var directionsService = new google.maps.DirectionsService();
        var directionsDisplay = new google.maps.DirectionsRenderer();
        
        function init() {
 
    var mapOptions = {
      zoom: 13,
      center: center,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
     
    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
     
    makeRequest('getIncidencias.php', function(data) {
             
        var data = JSON.parse(data.responseText);
         
        for (var i = 0; i < data.length; i++) {
            displayLocation(data[i]);
        }
    });
}
        
        function displayLocation(location) {
        
            
                
                var position = new google.maps.LatLng(parseFloat(location.latitud), parseFloat(location.longitud));
                var marker = new google.maps.Marker({
                    map: map, 
                    position: position,
                    title: location.name_inc
                });
                
                
            
        }
        
        function makeRequest(url, callback) {
            var request;
            if (window.XMLHttpRequest) {
                request = new XMLHttpRequest(); // IE7+, Firefox, Chrome, Opera, Safari
            } else {
                request = new ActiveXObject("Microsoft.XMLHTTP"); // IE6, IE5
            }
            request.onreadystatechange = function() {
                if (request.readyState == 4 && request.status == 200) {
                    callback(request);
                }
            }
            request.open("GET", url, true);
            request.send();
        }
        //]]>
        </script>

    </head>

    <body onload="init();">

        <div class="Table">
            <h1>Incidencias</h1>
            <br></br>
            <br></br>
            <?php
                echo "<table border=1 >"; 
                echo "<tr>"; 
                echo "<td><b>Nombre</b></td>"; 
                echo "<td><b>Fecha</b></td>"; 
                echo "<td><b>Comentario</b></td>"; 
                echo "<td><b>Id Usuario</b></td>"; 
                echo "<td><b>Latitud</b></td>"; 
                echo "<td><b>Longitud</b></td>"; 
                echo "<td><b>Imagen</b></td>"; 
                echo "<td><b>Estado</b></td>"; 
                echo "<td><b>Mostrar</b></td>"; 
                echo "</tr>"; 
                while($row = mysql_fetch_array($result)){ 
                    foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
                    echo "<tr>";  
                    echo "<td width=\"10%\"><font face=\"verdana\">" . utf8_encode($row["name_inc"]) . "</font></td>";
                    echo "<td width=\"10%\"><font face=\"verdana\">" . $row["fecha"] . "</font></td>";
	                echo "<td width=\"10%\"><font face=\"verdana\">" . utf8_encode($row["comentario"]) . "</font></td>";
	                echo "<td width=\"10%\"><font face=\"verdana\">" . $row["id_usr"] . "</font></td>";
	                echo "<td width=\"10%\"><font face=\"verdana\">" . $row["latitud"] . "</font></td>";
	                echo "<td width=\"10%\"><font face=\"verdana\">" . $row["longitud"] . "</font></td>";
	                echo "<td width=\"10%\"><font face=\"verdana\">" . "<a href=\"CityBugs/imagenes/".$row["imagen"]."\">".$row["imagen"]. "</a></font></td>";	
                    echo "<td width=\"10%\"><font face=\"verdana\">" . $row["estado"]. "</font></td>";
                    echo "<td valign='top'><a href=mostrarIncidencia.php?_id={$row['_id']}>Mostar</a></td></tr>"; 
                    echo "</tr>"; 
                } 
        echo "</table>"; 
        ?>
            
        </div>

        <div id="map_canvas" align="center" style="width: 70%; height: 500px;margin:auto"></div>
        <!-- Javascript -->
        <script src="assets/js/jquery-1.8.2.min.js"></script>
        <script src="assets/js/supersized.3.2.7.min.js"></script>
        <script src="assets/js/supersized-init.js"></script>
        <script src="assets/js/scripts.js"></script>

    </body>

</html>