<? 
include('conectar.php'); 
if (isset($_GET['_id']) ) { 
$_id = (int) $_GET['_id']; 
if (isset($_POST['submitted'])) { 
foreach($_POST AS $key => $value) { $_POST[$key] = mysql_real_escape_string($value); } 
$sql = "UPDATE `incidencia` SET  `name_inc` =  '{$_POST['name_inc']}' ,  `fecha` =  '{$_POST['fecha']}' ,  `comentario` =  '{$_POST['comentario']}' ,  `id_usr` =  '{$_POST['id_usr']}' ,  `latitud` =  '{$_POST['latitud']}' ,  `longitud` =  '{$_POST['longitud']}' ,  `imagen` =  '{$_POST['imagen']}' ,  `estado` =  '{$_POST['estado']}'   WHERE `_id` = '$_id' "; 
mysql_query($sql) or die(mysql_error()); 
echo (mysql_affected_rows()) ? "Incidencia Editada.<br />" : "Ningun Cambio. <br />"; 
echo "<a href='listarIncidencia.php'>Volver a la Lista</a>"; 
} 
$row = mysql_fetch_array ( mysql_query("SELECT * FROM `incidencia` WHERE `_id` = '$_id' ")); 
?>

<? } ?> 

<!DOCTYPE html>
<html lang="es" class="no-js">

    <head>

        <meta charset="utf-8">
        <title>Incidencia</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- CSS -->
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=PT+Sans:400,700'>
        <link rel="stylesheet" href="assets/css/reset.css">
        <link rel="stylesheet" href="assets/css/supersized.css">
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>
        <script>
            function initialize() {
                var myLatlng = new google.maps.LatLng(<?= stripslashes($row['latitud']) ?>,<?= stripslashes($row['longitud']) ?>);
                var mapOptions = {
                    zoom: 12,
                    center: myLatlng
                }
                var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map,
                    title: '<?= stripslashes($row['name_inc']) ?>'
                });
            }

            google.maps.event.addDomListener(window, 'load', initialize);
    </script>

    </head>

    <body>

        <div class="page-container">
            <h1>Incidencia</h1>
            <form action="" method="post">
                <input type="text" name="name_inc" class="name" value='Nombre: <?= stripslashes($row['name_inc']) ?>' disabled/>
                <input type="text" name="fecha" class="fecha" value='Fecha: <?= stripslashes($row['fecha']) ?>' disabled/>
                <input type="text" name="comentario" class="comentario" value='Comentario: <?= stripslashes($row['comentario']) ?>' disabled/>
                <input type="text" name="id_usr" class="iduser" value='Usuario: <?= stripslashes($row['id_usr']) ?>' disabled/>
                <input type="text" name="latitud" class="latitud" value='Latitud: <?= stripslashes($row['latitud']) ?>' disabled/>
                <input type="text" name="longitud" class="longitud" value='Longitud: <?= stripslashes($row['longitud']) ?>' disabled/> 
                <input type="text" name="imagen" class="imagen" value='Imagen: <?= stripslashes($row['imagen']) ?>' disabled/>
                <input type="text" name="estado" class="estado" value='Estado: <?= stripslashes($row['estado']) ?>' disabled/>
                <br></br>
            </form>

            <img src="/CityBugs/imagenes/<?= stripslashes($row['imagen']) ?>" border="1" alt="Incidencia <?= stripslashes($row['name_inc']) ?> " width="400" height="500" >
            <br></br>    
            <div id="map-canvas" align="center" style="width:400px;height:500px;margin:auto"></div>
            <br></br>    
            <div class="error"><span>+</span></div>
                
                
        </div>

        <!-- Javascript -->
        <script src="assets/js/jquery-1.8.2.min.js"></script>
        <script src="assets/js/supersized.3.2.7.min.js"></script>
        <script src="assets/js/supersized-init.js"></script>
        <script src="assets/js/scripts.js"></script>

    </body>

</html>