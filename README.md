# **Citybugs**

Proyecto para la asignatura Gestión de Sistemas de la Información (2014-2015) de la Escuela Superior de Informática de Ciudad Real [ESI](http://webpub.esi.uclm.es/).

### **¿Qué es Citybugs?** ###

Es una aplicación Android mediante la cual los usuarios pueden hacer reportes de problemas en su ciudad, aportando información sobre la localización de éstos para servir de ayuda.

### **Desarrollado por:** ###

* [Álvaro Fernández Villa](https://bitbucket.org/afdezvilla/)
* [Maria del Pilar Moreno Duque](https://bitbucket.org/iammaripi/)

### **Documentación** ###

Puede encontrar toda la documentación necesaria en las diferentes secciones de la [wiki del proyecto](https://bitbucket.org/almadeveloping/citybugs/wiki/Home).

![Citybugs.png](https://bitbucket.org/repo/eaypyL/images/1965448543-Citybugs.png)