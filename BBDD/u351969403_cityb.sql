
-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 08-05-2015 a las 13:56:56
-- Versión del servidor: 5.1.71
-- Versión de PHP: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `u351969403_cityb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `incidencia`
--

CREATE TABLE IF NOT EXISTS `incidencia` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `name_inc` varchar(30) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `fecha` varchar(30) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `comentario` varchar(50) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `id_usr` int(11) NOT NULL,
  `latitud` varchar(20) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `longitud` varchar(20) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `imagen` varchar(20) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `estado` varchar(10) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Volcado de datos para la tabla `incidencia`
--

INSERT INTO `incidencia` (`_id`, `name_inc`, `fecha`, `comentario`, `id_usr`, `latitud`, `longitud`, `imagen`, `estado`) VALUES
(1, 'Alu', '20/04/2015', 'Mesa rota', 2, '38.9899807', '-3.9205313', '2_1429541223563.jpg', 'informada'),
(3, 'Ratones', '27/04/2015', 'Ratones', 1, '38.9899924', '-3.9205416', '1_1430144883671.jpg', 'informada'),
(5, 'Politecnico inundado', '28/04/2015', 'Mucha agua', 1, '38.9915572', '-3.91925', '1_1430208157691.jpg', 'informada'),
(6, 'Farola rota', '28/04/2015', 'farola', 1, '38.9914719', '-3.9196362', '1_1430209645222.jpg', 'informada'),
(24, 'Prueba', '01/05/2015', 'Prueba', 1, '38.6896668', '-4.1144888', '1_1430507802905.jpg', 'informada'),
(9, 'llegada', '28/04/2015', 'puertollano', 3, '38.6978814', '-4.1081913', '3_1430245447400.jpg', 'informada');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(15) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `ape` varchar(15) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `pass` varchar(20) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `email` varchar(50) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `phone` varchar(12) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`_id`, `name`, `ape`, `pass`, `email`, `phone`) VALUES
(1, 'admin', 'admin', 'admin', 'admin@admin.es', '555-555-555'),
(2, 'alvaro', 'fdez villa', 'holi', 'alvaro.fernandez6@alu.uclm.es', '555-555-555'),
(3, 'maripi', 'moreno duque', 'holi', 'mpilar.moreno@alu.uclm.es', '555-555-555'),
(5, 'usuario', 'usuario de prue', 'usuario', 'user@hotmail.com', '55555');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
